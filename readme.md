Minikube start
kubectl apply -f deployment.yml
kubectl get services


docker build -t rnauka/banktransfer:v1.3 .

docker push rnauka/banktransfer:v1.3

minikube service --url  banktransferservice

minikube grant to all accounts full access (https://kubernetes.io/docs/reference/access-authn-authz/rbac/#service-account-permissions):
kubectl create clusterrolebinding serviceaccounts-cluster-admin \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts

helm install rel29dec3 ./bankchart
