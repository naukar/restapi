package validator;

import java.time.LocalDate;

public class SettlementDateValidator implements Validator<LocalDate> {

    public static final String EMPTY_DATE = "EMPTY_SETTLEMENT_DATE";
    public static final String NOT_VALID_DATE = "SETTLEMENT_DATE_IN_PAST";

    @Override
    public ValidationResult isValid(LocalDate settlementDate) {
        if (settlementDate == null) {
            return new ValidationResult(EMPTY_DATE, false);
        }

        if (LocalDate.now().isAfter(settlementDate)) {
            return new ValidationResult(NOT_VALID_DATE, false);
        }

        return ValidationResult.success();
    }

}
