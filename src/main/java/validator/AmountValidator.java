package validator;

import java.math.BigDecimal;

public class AmountValidator implements Validator<BigDecimal> {
    public static final String EMPTY_AMOUNT = "EMPTY_AMOUNT";
    public static final String NOT_VALID_AMOUNT = "NOT_VALID_AMOUNT";

    @Override
    public ValidationResult isValid(BigDecimal amount) {
        if (amount == null) {
            return new ValidationResult(EMPTY_AMOUNT, false);
        }

        if (BigDecimal.ONE.compareTo(amount) > 0) {
            return new ValidationResult(NOT_VALID_AMOUNT, false);
        }

        return ValidationResult.success();
    }
}
