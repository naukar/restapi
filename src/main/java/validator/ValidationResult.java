package validator;

public class ValidationResult {
    private String message;
    private Boolean valid;

    public ValidationResult(String message, Boolean valid) {
        this.message = message;
        this.valid = valid;
    }

    public static ValidationResult success() {
        return new ValidationResult("", true);
    }

    public String getMessage() {
        return message;
    }

    public Boolean isValid() {
        return valid;
    }
}
