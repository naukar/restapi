package validator;

public class AccountValidator implements Validator<String> {

    public static final String EMPTY_ACCOUNT = "EMPTY_ACCOUNT";
    public static final String NOT_VALID_ACCOUNT = "NOT_VALID_ACCOUNT";
    private static final int VALID_ACCOUNT_LENGTH = 8;

    @Override
    public ValidationResult isValid(String account) {
        if (account == null || account.isBlank()) {
            return new ValidationResult(EMPTY_ACCOUNT, false);
        }

        if (account.length() != VALID_ACCOUNT_LENGTH) {
            return new ValidationResult(NOT_VALID_ACCOUNT, false);
        }

        return ValidationResult.success();
    }

}
