package validator;

import command.CreateTransfer;

import javax.inject.Inject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateTransferValidator implements Validator<CreateTransfer> {

    private final Map<String, Validator> validators;

    @Inject
    public CreateTransferValidator(Map<String, Validator> validators) {
        this.validators = Map.copyOf(validators);
    }

    public ValidationResult isValid(CreateTransfer createTransfer) {
        Map properties = getProperties(createTransfer);

        List<String> messages = new ArrayList<>();
        properties.forEach((key, value) -> {
            if (!validators.containsKey(key)) {
                return;
            }
            ValidationResult result = validators.get(key).isValid(value);
            if (!result.isValid()) {
                messages.add(result.getMessage());
            }
        });

        return new ValidationResult(String.join(",", messages), messages.isEmpty());
    }

    private Map getProperties(CreateTransfer createTransfer) {
        Map properties = new HashMap();
        for (Field declaredField : createTransfer.getClass().getDeclaredFields()) {
            declaredField.setAccessible(true);
            try {
                properties.put(declaredField.getName(), declaredField.get(createTransfer));
            } catch (IllegalAccessException e) {
                throw new IllegalStateException("Could not read property of CreateTransfer object");
            }
        }
        return properties;
    }
}
