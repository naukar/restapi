package event;

import domain.events.TransferSubmitted;

import javax.inject.Inject;
import java.util.concurrent.SubmissionPublisher;

public class TransferSubmittedDispatcher implements EventDispatcher<TransferSubmitted> {

    private final SubmissionPublisher<TransferSubmitted> publisher;

    @Inject
    public TransferSubmittedDispatcher(SubmissionPublisher<TransferSubmitted> publisher) {
        this.publisher = publisher;
    }

    @Override
    public void submit(TransferSubmitted event) {
        publisher.submit(event);
    }
}
