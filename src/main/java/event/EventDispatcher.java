package event;

public interface EventDispatcher<T> {
    void submit(T event);
}
