package repository;

import java.util.List;

public interface Repository<T> {
    int save(T transfer);

    T findById(int id);

    List<T> getAll();
}
