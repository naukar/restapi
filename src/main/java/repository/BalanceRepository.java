package repository;

import domain.Balance;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class BalanceRepository {

    private ConcurrentMap<String, BigDecimal> balances = new ConcurrentHashMap<>();

    public Balance getBalance(String account) {
        BigDecimal balance = balances.get(account);
        if (balance == null) {
            return null;
        }

        return new Balance(account, balance);
    }

    public void changeBy(String account, BigDecimal amount) {

        BigDecimal currentValue = balances.get(account);
        if (currentValue == null) {
            balances.putIfAbsent(account, amount);
        } else {
            BigDecimal newValue = currentValue.add(amount);
            balances.replace(account, currentValue, newValue);
        }
    }
}
