package repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.google.common.collect.Lists;
import domain.Transfer;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DynamoTransferRepository implements Repository<Transfer> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.BASIC_ISO_DATE;
    private final Table table;
    private final AtomicInteger sequence = new AtomicInteger(0);

    public DynamoTransferRepository(AmazonDynamoDB client) {
        table = new DynamoDB(client).getTable("Transfer");
    }

    @Override
    public int save(Transfer transfer) {
        Item item = new Item()
                .withPrimaryKey("id", sequence.incrementAndGet())
                .withString("recipientAccount", transfer.getRecipientAccount())
                .withString("recipient", transfer.getRecipient())
                .withString("account", transfer.getAccount())
                .withString("reference", transfer.getReference())
                .withNumber("amount", transfer.getAmount())
                .withString("settlementDate", transfer.getSettlementDate().format(FORMATTER));
        table.putItem(item);
        return sequence.get();
    }

    @Override
    public Transfer findById(int id) {
        Item item = table.getItem("id", id);

        LocalDate settlementDate = LocalDate.parse(item.getString("settlementDate"), FORMATTER);
        Transfer transfer = new Transfer(
                item.getString("account"),
                item.getString("recipientAccount"),
                item.getNumber("amount"),
                settlementDate,
                item.getString("recipient"),
                item.getString("reference")
        );
        return new Transfer(id, transfer);
    }

    @Override
    public List<Transfer> getAll() {
        List<Transfer> result = Lists.newArrayList();
        table.scan().forEach(item -> {

            LocalDate settlementDate = LocalDate.parse(item.getString("settlementDate"), FORMATTER);
            Transfer transfer = new Transfer(
                    item.getString("account"),
                    item.getString("recipientAccount"),
                    item.getNumber("amount"),
                    settlementDate,
                    item.getString("recipient"),
                    item.getString("reference")
            );
            result.add(new Transfer(item.getInt("id"), transfer));
        });
        return result;
    }
}
