package repository;

import domain.Transfer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InMemoryTransferRepository implements Repository<Transfer> {

    private final AtomicInteger sequence = new AtomicInteger(0);
    private final Map<Integer, Transfer> storage = new ConcurrentHashMap();

    @Override
    public int save(Transfer transfer) {
        int id = sequence.incrementAndGet();
        storage.put(id, new Transfer(id, transfer));

        return id;
    }

    @Override
    public Transfer findById(int id) {
        return storage.get(id);
    }

    @Override
    public List<Transfer> getAll() {
        return new ArrayList<>(storage.values());
    }
}
