import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import di.TransferModule;
import http.RestRequestHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

class Application {

    public static void main(String[] args) throws IOException {
        Injector injector = Guice.createInjector(new TransferModule());
        HttpHandler transferRequestHandler = injector
                .getInstance(Key.get(HttpHandler.class, Names.named("transferRequestHandler")));

        HttpHandler balanceRequestHandler = injector
                .getInstance(Key.get(HttpHandler.class, Names.named("balanceRequestHandler")));

        TransferProperties properties = new TransferProperties("application.properties");

        HttpServer server = HttpServer.create(new InetSocketAddress(properties.getInteger("server.port")), 0);
        server.createContext("/api/1/transfer", transferRequestHandler);
        server.createContext("/api/1/balance", balanceRequestHandler);
        server.setExecutor(Executors.newFixedThreadPool(properties.getInteger("server.threads")));
        server.start();
    }
}
