package di;

import http.RequestProcessor;
import http.RestAction;
import http.RestRequestHandler;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

public class TransferRestRequestHandler extends RestRequestHandler {

    @Inject
    public TransferRestRequestHandler(
            @Named("transferProcessors") Map<RestAction, RequestProcessor> processors, @Named("transferBaseUrl") String baseUrl) {
        super(processors, baseUrl);
    }
}
