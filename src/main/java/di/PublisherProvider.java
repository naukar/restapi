package di;

import com.google.inject.Provider;
import domain.Transfer;
import domain.events.TransferSubmitted;
import gateway.ClearingTransferGateway;
import repository.BalanceRepository;
import repository.Repository;

import javax.inject.Inject;
import java.util.concurrent.SubmissionPublisher;

public class PublisherProvider implements Provider<SubmissionPublisher<TransferSubmitted>> {

    @Inject
    private BalanceRepository balanceRepository;

    @Inject
    private Repository<Transfer> transferRepository;

    @Override
    public SubmissionPublisher<TransferSubmitted> get() {
        SubmissionPublisher<TransferSubmitted> publisher = new SubmissionPublisher<>();
        publisher.subscribe(new ClearingTransferGateway(balanceRepository, transferRepository));
        return publisher;
    }
}
