package di;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import com.sun.net.httpserver.HttpHandler;
import command.CreateTransfer;
import converter.Converter;
import converter.FromJsonConverter;
import converter.IntegerConverter;
import converter.ToJsonConverter;
import domain.Balance;
import domain.Transfer;
import domain.events.TransferSubmitted;
import event.EventDispatcher;
import event.TransferSubmittedDispatcher;
import gateway.ClearingTransferGateway;
import http.*;
import repository.BalanceRepository;
import repository.InMemoryTransferRepository;
import repository.Repository;
import validator.*;

import java.util.List;
import java.util.concurrent.SubmissionPublisher;

public class TransferModule extends AbstractModule {

    @Override
    protected void configure() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        bind(ObjectMapper.class).toInstance(objectMapper);

        bind(BalanceRepository.class).toProvider(new BalanceRepositoryProvider());
        bind(new TypeLiteral<SubmissionPublisher<TransferSubmitted>>() {}).toProvider(new PublisherProvider());

        bind(new TypeLiteral<Class<CreateTransfer>>() {}).toInstance(CreateTransfer.class);
        bind(new TypeLiteral<Validator<CreateTransfer>>() {}).to(CreateTransferValidator.class);
        bind(new TypeLiteral<Repository<Transfer>>() {}).toInstance(new InMemoryTransferRepository());
        bind(new TypeLiteral<Converter<String, CreateTransfer>>() {}).to(new TypeLiteral<FromJsonConverter<CreateTransfer>>(){});
        bind(new TypeLiteral<Converter<List<Transfer>, String>>() {}).to(new TypeLiteral<ToJsonConverter<List<Transfer>>>(){});
        bind(new TypeLiteral<Converter<Transfer, String>>() {}).to(new TypeLiteral<ToJsonConverter<Transfer>>(){});
        bind(new TypeLiteral<Converter<Balance, String>>() {}).to(new TypeLiteral<ToJsonConverter<Balance>>(){});
        bind(new TypeLiteral<Converter<String, Integer>>() {}).to(IntegerConverter.class);
        bind(new TypeLiteral<EventDispatcher<TransferSubmitted>>() {}).to(TransferSubmittedDispatcher.class);

        MapBinder<String, Validator> validatorsMap = MapBinder.newMapBinder(binder(), String.class, Validator.class);
        validatorsMap.addBinding("settlementDate").to(SettlementDateValidator.class);
        validatorsMap.addBinding("account").to(AccountValidator.class);
        validatorsMap.addBinding("recipientAccount").to(AccountValidator.class);
        validatorsMap.addBinding("amount").to(AmountValidator.class);

        MapBinder<RestAction, RequestProcessor> requestProcessors = MapBinder.newMapBinder(binder(), RestAction.class, RequestProcessor.class, Names.named("transferProcessors"));
        requestProcessors.addBinding(RestAction.CREATE).to(CreateTransferProcessor.class);
        requestProcessors.addBinding(RestAction.LIST).to(GetTransfersProcessor.class);
        requestProcessors.addBinding(RestAction.GET).to(GetTransferProcessor.class);

        MapBinder<RestAction, RequestProcessor> balanceProcessors = MapBinder.newMapBinder(binder(), RestAction.class, RequestProcessor.class, Names.named("balanceProcessors"));
        balanceProcessors.addBinding(RestAction.GET).to(GetBalanceProcessor.class);

        bind(String.class).annotatedWith(Names.named("transferBaseUrl")).toInstance("/api/1/transfer");
        bind(String.class).annotatedWith(Names.named("balanceBaseUrl")).toInstance("/api/1/balance");

        bind(HttpHandler.class).annotatedWith(Names.named("transferRequestHandler"))
                .to(TransferRestRequestHandler.class);

        bind(HttpHandler.class).annotatedWith(Names.named("balanceRequestHandler"))
                .to(BalanceRestRequestHandler.class);
    }
}
