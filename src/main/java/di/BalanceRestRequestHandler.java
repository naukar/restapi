package di;

import http.RequestProcessor;
import http.RestAction;
import http.RestRequestHandler;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

public class BalanceRestRequestHandler extends RestRequestHandler {

    @Inject
    public BalanceRestRequestHandler(
            @Named("balanceProcessors") Map<RestAction, RequestProcessor> processors, @Named("balanceBaseUrl") String baseUrl) {
        super(processors, baseUrl);
    }
}
