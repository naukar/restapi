package di;

import com.google.inject.Provider;
import repository.BalanceRepository;

public class BalanceRepositoryProvider implements Provider<BalanceRepository> {

    private BalanceRepository balanceRepository = new BalanceRepository();

    @Override
    public BalanceRepository get() {
        return balanceRepository;
    }
}
