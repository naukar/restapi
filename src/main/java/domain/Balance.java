package domain;

import java.math.BigDecimal;
import java.util.Objects;

public class Balance {
    private final String account;
    private final BigDecimal balance;

    public Balance(String account, BigDecimal balance) {
        this.account = account;
        this.balance = balance;
    }

    public String getAccount() {
        return account;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Balance balance1 = (Balance) o;
        return Objects.equals(account, balance1.account) &&
                Objects.equals(balance, balance1.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, balance);
    }
}
