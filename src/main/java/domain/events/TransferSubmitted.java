package domain.events;

public class TransferSubmitted {
    private int transferId;

    public TransferSubmitted(int transferId) {
        this.transferId = transferId;
    }

    public int getTransferId() {
        return transferId;
    }

    @Override
    public String toString() {
        return "TransferSubmitted{" +
                "transferId=" + transferId +
                '}';
    }
}
