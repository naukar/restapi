package domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Transfer {
    private final String account;
    private final String recipientAccount;
    private final BigDecimal amount;
    private final LocalDate settlementDate;
    private final String recipient;
    private final String reference;
    private final int id;

    public Transfer(String account, String recipientAccount, BigDecimal amount, LocalDate settlementDate, String recipient, String reference) {
        this.account = account;
        this.recipientAccount = recipientAccount;
        this.amount = amount;
        this.settlementDate = settlementDate;
        this.recipient = recipient;
        this.reference = reference;
        this.id = 0;
    }

    public Transfer(int id, Transfer transfer) {
        this.account = transfer.getAccount();
        this.recipientAccount = transfer.getRecipientAccount();
        this.amount = transfer.getAmount();
        this.settlementDate = transfer.getSettlementDate();
        this.recipient = transfer.getRecipient();
        this.reference = transfer.getReference();
        this.id = id;
    }

    public String getRecipientAccount() {
        return recipientAccount;
    }

    public int getId() {
        return id;
    }

    public String getAccount() {
        return account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getReference() {
        return reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transfer transfer = (Transfer) o;
        return id == transfer.id &&
                Objects.equals(account, transfer.account) &&
                Objects.equals(amount, transfer.amount) &&
                Objects.equals(settlementDate, transfer.settlementDate) &&
                Objects.equals(recipient, transfer.recipient) &&
                Objects.equals(reference, transfer.reference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, amount, settlementDate, recipient, reference, id);
    }
}
