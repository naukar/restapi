package converter;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;

public class ToJsonConverter<F> implements Converter<F, String> {
    private final ObjectMapper mapper;

    @Inject
    public ToJsonConverter(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public String convert(F object) throws ConversionException {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new ConversionException(e);
        }
    }
}
