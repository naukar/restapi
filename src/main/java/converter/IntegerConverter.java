package converter;

public class IntegerConverter implements Converter<String, Integer> {
    @Override
    public Integer convert(String value) throws ConversionException {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(e);
        }
    }
}
