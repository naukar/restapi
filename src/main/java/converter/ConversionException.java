package converter;

public class ConversionException extends Exception {

    public ConversionException(Throwable throwable) {
        super(throwable);
    }
}
