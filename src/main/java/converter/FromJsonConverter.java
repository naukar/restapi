package converter;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;

public class FromJsonConverter<T> implements Converter<String, T> {
    private final ObjectMapper mapper;
    private final Class<T> clazz;

    @Inject
    public FromJsonConverter(ObjectMapper mapper, Class<T> clazz) {
        this.mapper = mapper;
        this.clazz = clazz;
    }

    @Override
    public T convert(String object) throws ConversionException {
        try {
            return mapper.readValue(object, clazz);
        } catch (JsonProcessingException e) {
            throw new ConversionException(e);
        }
    }
}
