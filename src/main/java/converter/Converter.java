package converter;

public interface Converter<F, T> {
    T convert(F value) throws ConversionException;
}
