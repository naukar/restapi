package command.handler;

import command.CreateTransfer;
import domain.Transfer;
import domain.events.TransferSubmitted;
import event.EventDispatcher;
import repository.Repository;
import validator.ValidationResult;
import validator.Validator;

import javax.inject.Inject;

public class CreateTransferHandler {

    private final Validator<CreateTransfer> validator;
    private final Repository<Transfer> repository;
    private final EventDispatcher<TransferSubmitted> publisher;

    @Inject
    public CreateTransferHandler(Validator<CreateTransfer> validator,
                                 Repository<Transfer> repository,
                                 EventDispatcher<TransferSubmitted> publisher) {
        this.validator = validator;
        this.repository = repository;
        this.publisher = publisher;
    }

    public CreateTransferResult handle(CreateTransfer createTransfer) {
        ValidationResult validationResult = validator.isValid(createTransfer);
        if (!validationResult.isValid()) {
            return new CreateTransferResult(false, validationResult.getMessage());
        }

        int transferId = repository.save(new Transfer(
                createTransfer.getAccount(),
                createTransfer.getRecipientAccount(),
                createTransfer.getAmount(),
                createTransfer.getSettlementDate(),
                createTransfer.getRecipient(),
                createTransfer.getReference()
        ));


        publisher.submit(new TransferSubmitted(transferId));

        return CreateTransferResult.success();
    }
}
