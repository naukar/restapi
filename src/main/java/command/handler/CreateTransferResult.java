package command.handler;

public class CreateTransferResult {
    private final boolean success;
    private final String message;

    public CreateTransferResult(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public static CreateTransferResult success() {
        return new CreateTransferResult(true, "");
    }

    public boolean isSuccessful() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
