package command;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CreateTransfer {
    private final String recipient;
    private final String account;
    private final String reference;
    private final LocalDate settlementDate;
    private final BigDecimal amount;
    private final String recipientAccount;

    private CreateTransfer() {
        this.recipient = null;
        this.account = null;
        this.reference = null;
        this.settlementDate = null;
        this.amount = null;
        this.recipientAccount = null;
    }

    public CreateTransfer(String recipient, String account, String recipientAccount, String reference, LocalDate settlementDate, BigDecimal amount) {
        this.recipient = recipient;
        this.account = account;
        this.recipientAccount = recipientAccount;
        this.reference = reference;
        this.settlementDate = settlementDate;
        this.amount = amount;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getAccount() {
        return account;
    }

    public String getReference() {
        return reference;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getRecipientAccount() {
        return recipientAccount;
    }
}
