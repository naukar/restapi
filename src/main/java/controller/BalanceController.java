package controller;

import domain.Balance;
import service.BalanceService;

import javax.inject.Inject;
import java.util.Optional;

public class BalanceController {

    private final BalanceService balanceService;

    @Inject
    public BalanceController(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    public Optional<Balance> get(String account) {
        return balanceService.get(account);
    }
}
