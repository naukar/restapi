package controller;

import command.CreateTransfer;
import command.handler.CreateTransferHandler;
import command.handler.CreateTransferResult;
import domain.Transfer;
import http.Response;
import service.TransferService;

import javax.inject.Inject;

import java.util.List;
import java.util.Optional;

import static http.Response.*;

public class TransferController {
    private final CreateTransferHandler createTransferHandler;
    private final TransferService transferService;

    @Inject
    public TransferController(CreateTransferHandler createTransferHandler, TransferService transferService) {
        this.createTransferHandler = createTransferHandler;
        this.transferService = transferService;
    }

    public Response<String> create(CreateTransfer createTransfer) {
        CreateTransferResult result = createTransferHandler.handle(createTransfer);

        if (!result.isSuccessful()) {
            return badRequest(result.getMessage());
        }

        return created();
    }

    public Response<List<Transfer>> getAll() {
        return new Response<>(Response.SUCCESS, transferService.getAll());
    }

    public Optional<Transfer> get(Integer id) {
        return transferService.get(id);
    }
}
