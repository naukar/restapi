package service;

import domain.Balance;
import repository.BalanceRepository;

import javax.inject.Inject;
import java.util.Optional;

public class BalanceService {
    private final BalanceRepository balanceRepository;

    @Inject
    public BalanceService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public Optional<Balance> get(String account) {
        return Optional.ofNullable(balanceRepository.getBalance(account));
    }
}
