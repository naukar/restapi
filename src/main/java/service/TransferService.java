package service;

import domain.Transfer;
import repository.Repository;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class TransferService {
    private final Repository<Transfer> repository;

    @Inject
    public TransferService(Repository<Transfer> repository) {
        this.repository = repository;
    }

    public List<Transfer> getAll() {
        return repository.getAll();
    }

    public Optional<Transfer> get(Integer id) {
        return Optional.ofNullable(repository.findById(id));
    }
}
