package http;

import controller.BalanceController;
import converter.Converter;
import domain.Balance;
import domain.Transfer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.util.Optional;

import static http.Response.*;

public class GetBalanceProcessor implements RequestProcessor {

    private static final Logger LOGGER = LogManager.getLogger(GetBalanceProcessor.class);
    private static final String ID = "id";

    private final BalanceController balanceController;
    private final Converter<Balance, String> jsonConverter;

    @Inject
    public GetBalanceProcessor(BalanceController balanceController,
                               Converter<Balance, String> jsonConverter) {
        this.balanceController = balanceController;
        this.jsonConverter = jsonConverter;
    }

    @Override
    public Response<String> process(Request request) throws Exception {
        LOGGER.debug("Handling get request");

        Optional<Balance> transfer = balanceController.get(request.getParams().get(ID));
        if (!transfer.isPresent()) {
            return notFound();
        }

        return success(jsonConverter.convert(transfer.get()));
    }
}
