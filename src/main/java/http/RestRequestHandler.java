package http;

import com.google.common.collect.Lists;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.sun.jersey.api.uri.UriTemplate;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static http.RestAction.*;

public class RestRequestHandler implements HttpHandler {

    private static final Logger LOGGER = LogManager.getLogger(RestRequestHandler.class);
    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";

    private final Map<RestAction, RequestProcessor> processors;
    private final String baseUrl;

    @AssistedInject
    public RestRequestHandler(
           @Assisted Map<RestAction, RequestProcessor> processors,
           @Assisted String baseUrl) {
        this.processors = Map.copyOf(processors);
        this.baseUrl = baseUrl;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        Map<String, String> params = getParams(exchange.getRequestURI().getPath(), baseUrl);

        Response<String> response;
        try {
            String requestBody = getRequestBody(exchange);
            Request request = new Request(requestBody, params);

            if (hasProcessor(CREATE) && METHOD_POST.equals(requestMethod)) {
                response = callProcessor(CREATE, request);
            } else if (hasProcessor(GET) && METHOD_GET.equals(requestMethod) && params.containsKey("id")) {
                response = callProcessor(GET, request);
            } else if (hasProcessor(LIST) && METHOD_GET.equals(requestMethod)) {
                response = callProcessor(LIST, request);
            }  else {
                response = Response.methodNotAllowed();
            }
        } catch (Exception exception) {
            LOGGER.warn(exception);
            response = Response.internalError();
        }

        sendResponse(exchange, response);
    }

    private boolean hasProcessor(RestAction create) {
        return processors.containsKey(create);
    }

    private Response<String> callProcessor(RestAction create, Request request) throws Exception {
        return processors.get(create).process(request);
    }

    private Map<String, String> getParams(String path, String baseUrl) {
        Map<String, String> params = new HashMap<>();
        UriTemplate template = new UriTemplate(baseUrl + "/{id}");
        template.match(path, params);
        return params;
    }

    private String getRequestBody(HttpExchange exchange) throws IOException {
        return new String(exchange.getRequestBody().readAllBytes());
    }

    private void sendResponse(HttpExchange exchange, Response<String> response) throws IOException {
        exchange.getResponseHeaders().put("Content-Type", Lists.newArrayList("application/json"));
        exchange.sendResponseHeaders(response.getCode(), response.getBody().getBytes().length);
        OutputStream output = exchange.getResponseBody();
        output.write(response.getBody().getBytes());
        output.flush();
        exchange.close();
    }
}
