package http;

public enum RestAction {
    CREATE, LIST, GET
}
