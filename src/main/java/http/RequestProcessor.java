package http;

public interface RequestProcessor {

    Response<String> process(Request request) throws Exception;
}
