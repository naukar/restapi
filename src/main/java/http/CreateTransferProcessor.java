package http;

import command.CreateTransfer;
import controller.TransferController;
import converter.ConversionException;
import converter.Converter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;

public class CreateTransferProcessor implements RequestProcessor {

    private static final Logger LOGGER = LogManager.getLogger(CreateTransferProcessor.class);

    private final TransferController transferController;
    private final Converter<String, CreateTransfer> converter;

    @Inject
    public CreateTransferProcessor(TransferController transferController, Converter<String, CreateTransfer> converter) {
        this.transferController = transferController;
        this.converter = converter;
    }

    @Override
    public Response<String> process(Request request) {
        LOGGER.debug("Handling create request");

        CreateTransfer createTransfer;
        try {
            createTransfer = converter.convert(request.getBody());
        } catch (ConversionException exception) {
            return Response.badRequest();
        }

        return transferController.create(createTransfer);
    }
}
