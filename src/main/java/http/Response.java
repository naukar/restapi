package http;

import java.util.Objects;

public class Response<T> {

    public static final int SUCCESS = 200;
    public static final int CREATED = 201;
    public static final int BAD_REQUEST = 400;
    public static final int NOT_FOUND = 404;
    public static final int METHOD_NOT_ALLOWED = 405;
    public static final int INTERNAL_ERROR = 500;

    private final int code;
    private final T body;

    public Response(int code, T body) {
        this.code = code;
        this.body = body;
    }

    public static Response<String> created() {
        return new Response(CREATED, "");
    }

    public static Response<String> badRequest() {
        return new Response(BAD_REQUEST, "Bad Request or invalid parameters");
    }

    public static Response<String> internalError() {
        return new Response(INTERNAL_ERROR, "Unexpected error");
    }

    public static Response<String> badRequest(String message) {
        return new Response(INTERNAL_ERROR, message);
    }

    public static Response<String> success(String message) {
        return new Response(SUCCESS, message);
    }

    public static Response<String> methodNotAllowed() {
        return new Response<>(METHOD_NOT_ALLOWED, "Method not allowed");
    }

    public static Response<String> notFound() {
        return new Response<>(NOT_FOUND, "Not found");
    }

    public int getCode() {
        return code;
    }

    public T getBody() {
        return body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Response<?> response = (Response<?>) o;
        return code == response.code &&
                Objects.equals(body, response.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, body);
    }
}
