package http;

import controller.TransferController;
import converter.Converter;
import domain.Transfer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.util.Optional;

import static http.Response.*;

public class GetTransferProcessor implements RequestProcessor {

    private static final Logger LOGGER = LogManager.getLogger(GetTransferProcessor.class);
    private static final String ID = "id";

    private final TransferController transferController;
    private final Converter<Transfer, String> jsonConverter;
    private final Converter<String, Integer> integerConverter;

    @Inject
    public GetTransferProcessor(TransferController transferController,
                                Converter<Transfer, String> jsonConverter,
                                Converter<String, Integer> integerConverter) {
        this.transferController = transferController;
        this.jsonConverter = jsonConverter;
        this.integerConverter = integerConverter;
    }

    @Override
    public Response<String> process(Request request) throws Exception {
        LOGGER.debug("Handling get request");

        Integer id;
        try {
            id = integerConverter.convert(request.getParams().get(ID));
        } catch (Exception exception) {
            return badRequest();
        }

        Optional<Transfer> transfer = transferController.get(id);
        if (!transfer.isPresent()) {
            return notFound();
        }

        return success(jsonConverter.convert(transfer.get()));
    }
}
