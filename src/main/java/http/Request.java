package http;

import java.util.Map;
import java.util.Objects;

public class Request {
    private final String body;
    private final Map<String, String> params;

    public Request(String body, Map<String, String> params) {
        this.body = body;
        this.params = params;
    }

    public String getBody() {
        return body;
    }

    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Request request = (Request) o;
        return Objects.equals(body, request.body) &&
                Objects.equals(params, request.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(body, params);
    }

    @Override
    public String toString() {
        return "Request{" +
                "body='" + body + '\'' +
                ", params=" + params +
                '}';
    }
}
