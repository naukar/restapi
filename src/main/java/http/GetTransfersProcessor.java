package http;

import controller.TransferController;
import converter.Converter;
import domain.Transfer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.util.List;

import static http.Response.success;

public class GetTransfersProcessor implements RequestProcessor {

    private static final Logger LOGGER = LogManager.getLogger(GetTransfersProcessor.class);

    private final TransferController transferController;
    private final Converter<List<Transfer>, String> converter;

    @Inject
    public GetTransfersProcessor(TransferController transferController, Converter<List<Transfer>, String> converter) {
        this.transferController = transferController;
        this.converter = converter;
    }

    @Override
    public Response<String> process(Request request) throws Exception {
        LOGGER.debug("Handling get list request");

        Response<List<Transfer>> allTransfersResponse = transferController.getAll();
        LOGGER.debug("Converting: {}", allTransfersResponse.getBody().size());
        String json = converter.convert(allTransfersResponse.getBody());
        return Response.success(json);
    }
}
