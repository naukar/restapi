package http;

public enum RequestMethod {
    POST, GET, PUT, OPTIONS
}
