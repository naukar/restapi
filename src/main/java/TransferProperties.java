import java.io.IOException;
import java.util.Properties;

public class TransferProperties {

    private final String file;
    private Properties properties;

    public TransferProperties(String file) {
        this.file = file;
    }

    public Integer getInteger(String key) throws IOException {
        if (properties == null) {
            properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream(file));
        }

        return Integer.valueOf(properties.getProperty(key));
    }
}
