package gateway;

import domain.Transfer;
import domain.events.TransferSubmitted;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repository.BalanceRepository;
import repository.Repository;

import java.util.concurrent.Flow;

public class ClearingTransferGateway implements Flow.Subscriber<TransferSubmitted> {

    private static final Logger LOGGER = LogManager.getLogger(ClearingTransferGateway.class);
    private Flow.Subscription subscription;
    private final BalanceRepository balanceRepository;
    private final Repository<Transfer> transferRepository;

    public ClearingTransferGateway(BalanceRepository balanceRepository, Repository<Transfer> transferRepository) {
        this.balanceRepository = balanceRepository;
        this.transferRepository = transferRepository;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(TransferSubmitted event) {
        LOGGER.info("Got : {}", event);
        Transfer transfer = transferRepository.findById(event.getTransferId());
        balanceRepository.changeBy(transfer.getAccount(), transfer.getAmount().negate());
        balanceRepository.changeBy(transfer.getRecipientAccount(), transfer.getAmount());
    }

    @Override
    public void onError(Throwable throwable) {
        LOGGER.error("Error : ", throwable);
    }

    @Override
    public void onComplete() {
        LOGGER.debug("Done with processing");
    }
}
