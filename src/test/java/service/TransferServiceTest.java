package service;

import domain.Transfer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.Repository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransferServiceTest {

    @Test
    void shouldCallRepositoryToGetAllBankTransfers(@Mock Repository<Transfer> repository) {
        new TransferService(repository).getAll();

        verify(repository).getAll();
        verifyNoMoreInteractions(repository);
    }

    @Test
    void shouldCallRepositoryToGetAllBankTransfers(@Mock Repository<Transfer> repository, @Mock Transfer transfer) {
        Integer id = 1;
        when(repository.findById(id)).thenReturn(transfer);

        Optional<Transfer> optionalTransfer = new TransferService(repository).get(id);

        assertThat(optionalTransfer).isEqualTo(optionalTransfer);
        verify(repository).findById(id);
        verifyNoMoreInteractions(repository);
    }
}