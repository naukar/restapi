package controller;

import command.CreateTransfer;
import command.handler.CreateTransferHandler;
import command.handler.CreateTransferResult;
import domain.Transfer;
import http.Response;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import service.TransferService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class TransferControllerTest {

    private TransferController transferController;
    private TransferService transferService;
    private CreateTransferHandler createTransferHandler;

    @BeforeEach
    void init(@Mock CreateTransferHandler createTransferHandler, @Mock TransferService transferService) {
        this.createTransferHandler = createTransferHandler;
        this.transferService = transferService;
        transferController = new TransferController(createTransferHandler, transferService);
    }

    @Test
    void shouldCallHandlerToCreateObjectAndReturnCreated(@Mock CreateTransfer createTransfer) {
        when(createTransferHandler.handle(createTransfer)).thenReturn(CreateTransferResult.success());

        Response response = transferController.create(createTransfer);

        verify(createTransferHandler).handle(createTransfer);
        assertThat(response).isEqualToComparingFieldByField(Response.created());
    }

    @Test
    void shouldCallHandlerToCreateObjectAndReturnError(@Mock CreateTransfer createTransfer) {
        String error = "ERROR";
        when(createTransferHandler.handle(createTransfer)).thenReturn(new CreateTransferResult(false, error));

        Response response = transferController.create(createTransfer);

        verify(createTransferHandler).handle(createTransfer);
        assertThat(response).isEqualToComparingFieldByField(Response.badRequest(error));
    }

    @Test
    void shouldCallServiceToGetAllTransfers() {
        ArrayList<Transfer> transfers = Lists.newArrayList();
        when(transferService.getAll()).thenReturn(transfers);

        Response<List<Transfer>> response = transferController.getAll();

        assertThat(response.getBody()).isEqualTo(transfers);
        assertThat(response.getCode()).isEqualTo(Response.SUCCESS);
        verify(transferService).getAll();
    }

    @Test
    void shouldCallServiceToGetOneTransfers(@Mock Transfer transfer) {
        Integer id = 1;
        Optional<Transfer> optionalTransfer = Optional.of(transfer);
        when(transferService.get(id)).thenReturn(optionalTransfer);

        Optional<Transfer> response = transferController.get(id);

        assertThat(response).isEqualTo(optionalTransfer);
        verify(transferService).get(id);
    }
}