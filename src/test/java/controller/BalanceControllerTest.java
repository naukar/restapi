package controller;

import domain.Balance;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import service.BalanceService;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BalanceControllerTest {

    @Test
    void shouldCallBalanceServiceToGetBalance(@Mock BalanceService balanceService) {
        String account = "1";
        Balance balance = new Balance(account, BigDecimal.ONE);
        when(balanceService.get(account)).thenReturn(Optional.of(balance));

        Optional<Balance> optionalResponse = new BalanceController(balanceService).get(account);

        assertThat(optionalResponse.isPresent()).isEqualTo(true);
        assertThat(optionalResponse.get()).isEqualTo(balance);
        verify(balanceService).get(account);
    }
}