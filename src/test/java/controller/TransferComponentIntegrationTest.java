package controller;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static http.Response.BAD_REQUEST;
import static http.Response.SUCCESS;
import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

@Testcontainers
class TransferComponentIntegrationTest {

    private String host;

    @Container
    public GenericContainer jvm = new FixedHostPortGenericContainer("adoptopenjdk/openjdk11")
            .withExposedPorts(8000, 8000)
            .withClasspathResourceMapping("/transfer-jar-with-dependencies.jar", "/c/transfer-jar-with-dependencies.jar", BindMode.READ_ONLY)
            .withCommand("java -jar /c/transfer-jar-with-dependencies.jar");

    void createANewBankTransfer() throws IOException {
        given()
                .body(aMessage("src/test/resources/createtransfer.json"))
                .when()
                .post(host + "/api/1/transfer");
    }
    
    @BeforeEach
    public void setHost() {
        host = "http://" + jvm.getContainerIpAddress() + ":" + jvm.getFirstMappedPort();
    }

    @Test
    public void shouldGetAllBankTransfers() throws IOException {
        createANewBankTransfer();

        given()
                .body(aMessage("src/test/resources/createsecondtransfer.json"))
                .when()
                .post(host + "/api/1/transfer");

        expect()
                .given()
                .get(host + "/api/1/transfer")
                .then()
                .assertThat()
                .statusCode(SUCCESS)
                .body("account[0]", equalTo("12345678"))
                .body("amount[0]", equalTo(100))
                .body("account[1]", equalTo("12345678"))
                .body("amount[1]", equalTo(200))
                .body("id", equalTo(Lists.newArrayList(1, 2)));
    }

    @Test
    public void shouldGetDetailsAboutCreatedBankTransfers() throws IOException {
        createANewBankTransfer();

        expect()
                .given()
                .get(host + "/api/1/transfer/1")
                .then()
                .assertThat()
                .statusCode(SUCCESS)
                .body("account", equalTo("12345678"))
                .body("amount", equalTo(100));
    }

    @Test
    public void shouldGetBalanceForGivenAccount() throws IOException {
        createANewBankTransfer();

        expect()
                .given()
                .get(host + "/api/1/balance/12345678")
                .then()
                .assertThat()
                .statusCode(SUCCESS)
                .body("account", equalTo("12345678"))
                .body("balance", equalTo(-100));
    }

    @Test
    public void shouldFailToCreateANewBankTransferWhenWrongMessageSent() throws IOException {
        String message = aMessage("src/test/resources/createtransfer.wrong.json");

        given()
                .body(message)
                .when()
                .post(host + "/api/1/transfer")
                .then()
                .assertThat()
                .statusCode(BAD_REQUEST);
    }

    private static String aMessage(String s) throws IOException {
        return Files.readString(Paths.get(s));
    }
}