package repository;

import domain.Transfer;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class InMemoryTransferRepositoryTest {

    @Test
    void shouldSaveNewEntityAndGenerateId() {
        InMemoryTransferRepository inMemoryTransferRepository = new InMemoryTransferRepository();
        Transfer transfer = new Transfer("account", "recAccount", BigDecimal.ONE, LocalDate.now(), "recipient", "reference");

        int id = inMemoryTransferRepository.save(transfer);

        assertThat(id).isEqualTo(1);

        Transfer saved = inMemoryTransferRepository.findById(id);
        assertThat(saved).isEqualToIgnoringGivenFields(transfer, "id");
        assertThat(saved.getId()).isEqualTo(id);
    }

    @Test
    void shouldGetAllObjects() {
        Transfer transfer1 = new Transfer("account1", "recAccount1", BigDecimal.ONE, LocalDate.now(), "recipient", "reference");
        Transfer transfer2 = new Transfer("account2", "recAccount2", BigDecimal.TEN, LocalDate.now(), "recipient", "reference");

        InMemoryTransferRepository inMemoryTransferRepository = new InMemoryTransferRepository();
        inMemoryTransferRepository.save(transfer1);
        inMemoryTransferRepository.save(transfer2);

        List<Transfer> all = inMemoryTransferRepository.getAll();
        assertThat(all).hasSize(2);
        assertThat(all.get(0)).isEqualToIgnoringGivenFields(transfer1, "id");
        assertThat(all.get(1)).isEqualToIgnoringGivenFields(transfer2, "id");
    }

    @Test
    void shouldReturnNullObjectWhenObjectNotFound() {
        Transfer transfer = new InMemoryTransferRepository().findById(1);

        assertThat(transfer).isNull();
    }
}