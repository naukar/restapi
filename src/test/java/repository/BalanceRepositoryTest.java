package repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class BalanceRepositoryTest {

    static Stream<BigDecimal> values() {
        return Stream.of(BigDecimal.TEN, BigDecimal.TEN.negate(), BigDecimal.ZERO);
    }

    @ParameterizedTest
    @MethodSource("values")
    void shouldChangeBalanceWhenFirstOperation(BigDecimal value) {
        BalanceRepository balanceRepository = new BalanceRepository();
        String account = "1";
        balanceRepository.changeBy(account, value);

        assertThat(balanceRepository.getBalance(account).getBalance()).isEqualTo(value);
    }

    @ParameterizedTest
    @MethodSource("values")
    void shouldChangeNonZeroBalance(BigDecimal value) {
        BalanceRepository balanceRepository = new BalanceRepository();
        String account = "1";
        balanceRepository.changeBy(account, BigDecimal.ONE);
        balanceRepository.changeBy(account, value);

        assertThat(balanceRepository.getBalance(account).getBalance()).isEqualTo(value.add(BigDecimal.ONE));
    }

    @Test
    void shouldReturnNullWhenBalanceDoesNotExist() {
        assertThat(new BalanceRepository().getBalance("11")).isNull();
    }
}