package repository;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import domain.Transfer;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

@Testcontainers
public class DynamoTransferRepositoryTest {

    private static final int DYNAMO_PORT = 8000;

    private AmazonDynamoDB client;

    @Container
    public static GenericContainer dynamoDb =
            new GenericContainer("amazon/dynamodb-local:1.11.119")
                    .withExposedPorts(DYNAMO_PORT);

    @BeforeEach
    public void init() throws InterruptedException {
        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("http://" + dynamoDb.getContainerIpAddress()+":" + dynamoDb.getMappedPort(DYNAMO_PORT), "us-west-2"))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("rnauka", "dummy")))
                .build();

        if (!new DynamoDB(client).listTables().iterator().hasNext()) {
            Table table = new DynamoDB(client).createTable(
                    "Transfer", Lists.newArrayList(
                            new KeySchemaElement("id", KeyType.HASH)),

                    Lists.newArrayList(
                            new AttributeDefinition("id", ScalarAttributeType.N)
                    ),
                    new ProvisionedThroughput(5L, 5L)
            );
            table.waitForActive();
        }
    }

    @Test
    void shouldSaveNewEntityAndGenerateId() {
        DynamoTransferRepository dynamoTransferRepository = new DynamoTransferRepository(client);

        Transfer transfer = new Transfer("account", "recAccount", BigDecimal.ONE, LocalDate.now(), "recipient", "reference");

        int id = dynamoTransferRepository.save(transfer);

        assertThat(id).isEqualTo(1);

        Transfer saved = dynamoTransferRepository.findById(id);
        assertThat(saved).isEqualToIgnoringGivenFields(transfer, "id");
        assertThat(saved.getId()).isEqualTo(id);
    }

    @Test
    void shouldGetAllObjects() {
        Transfer transfer1 = new Transfer("account1", "recAccount1", BigDecimal.ONE, LocalDate.now(), "recipient", "reference");
        Transfer transfer2 = new Transfer("account2", "recAccount2", BigDecimal.TEN, LocalDate.now(), "recipient", "reference");

        DynamoTransferRepository dynamoTransferRepository = new DynamoTransferRepository(client);
        dynamoTransferRepository.save(transfer1);
        dynamoTransferRepository.save(transfer2);

        List<Transfer> all = dynamoTransferRepository.getAll();

        assertThat(all).hasSize(2);
        assertThat(all).extracting("account", "amount", "settlementDate", "recipient", "reference")
                .containsOnly(
                        tuple(transfer1.getAccount(), transfer1.getAmount(), transfer1.getSettlementDate(), transfer1.getRecipient(), transfer1.getReference()),
                        tuple(transfer2.getAccount(), transfer2.getAmount(), transfer2.getSettlementDate(), transfer2.getRecipient(), transfer2.getReference())
                );
    }


}
