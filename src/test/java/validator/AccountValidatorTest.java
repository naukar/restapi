package validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class AccountValidatorTest {

    private final AccountValidator validator = new AccountValidator();

    static Stream<String> blankStrings() {
        return Stream.of("", "   ", null);
    }

    @ParameterizedTest
    @MethodSource("blankStrings")
    void shouldReturnNotValidResultWhenAccountIsEmptyOrNull(final String account) {
        ValidationResult result = validator.isValid(account);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(AccountValidator.EMPTY_ACCOUNT);
    }

    static Stream<String> notValidAccounts() {
        return Stream.of("1234567", "123456789");
    }

    @ParameterizedTest
    @MethodSource("notValidAccounts")
    void shouldReturnNotValidResultWhenAccountTooLongOrTooShort(String account) {
        ValidationResult result = validator.isValid(account);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(AccountValidator.NOT_VALID_ACCOUNT);
    }

    @Test
    void shouldReturnValidResult() {
        ValidationResult result = validator.isValid("12345678");

        assertThat(result.isValid()).isTrue();
        assertThat(result.getMessage()).isBlank();
    }
}