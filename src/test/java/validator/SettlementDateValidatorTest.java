package validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class SettlementDateValidatorTest {

    private final SettlementDateValidator validator = new SettlementDateValidator();

    @Test
    void shouldReturnNotValidResultWhenDateIsNull() {
        ValidationResult result = validator.isValid(null);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(SettlementDateValidator.EMPTY_DATE);
    }

    @Test
    void shouldReturnNotValidResultWhenDateIsBeforeNow() {
        ValidationResult result = validator.isValid(LocalDate.now().minusDays(1));

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(SettlementDateValidator.NOT_VALID_DATE);
    }

    @Test
    void shouldReturnValidDate() {
        ValidationResult result = validator.isValid(LocalDate.now().minusDays(1));

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(SettlementDateValidator.NOT_VALID_DATE);
    }
}