package validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static java.math.BigDecimal.*;
import static org.assertj.core.api.Assertions.assertThat;

class AmountValidatorTest {

    private final AmountValidator validator = new AmountValidator();

    static Stream<BigDecimal> notValidAmount() {
        return Stream.of(ZERO, ONE.negate());
    }

    @ParameterizedTest
    @MethodSource("notValidAmount")
    void shouldReturnNotValidResultWhenAmountIsLessOrEqualToZero(BigDecimal amount) {
        ValidationResult result = validator.isValid(amount);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(AmountValidator.NOT_VALID_AMOUNT);
    }

    @Test
    void shouldReturnNotValidResultWhenNull() {
        ValidationResult result = validator.isValid(null);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo(AmountValidator.EMPTY_AMOUNT);
    }

    @Test
    void shouldReturnValidResult() {
        ValidationResult result = validator.isValid(ONE);

        assertThat(result.isValid()).isTrue();
    }
}