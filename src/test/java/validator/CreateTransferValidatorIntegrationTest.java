package validator;

import com.google.inject.Guice;
import com.google.inject.Injector;
import command.CreateTransfer;
import di.TransferModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class CreateTransferValidatorIntegrationTest {

    private CreateTransferValidator createTransferValidator;

    @BeforeEach
    void init () {
        Injector injector = Guice.createInjector(new TransferModule());
        createTransferValidator = injector.getInstance(CreateTransferValidator.class);
    }

    @Test
    void shouldReturnNotValidResult() {
        ValidationResult result = createTransferValidator.isValid(new CreateTransfer(null, null, null, null, null, null));

        assertThat(result.isValid()).isFalse();
        assertThat(result.getMessage()).isEqualTo("EMPTY_AMOUNT,EMPTY_ACCOUNT,EMPTY_SETTLEMENT_DATE,EMPTY_ACCOUNT");
    }

    @Test
    void shouldReturnValidResult() {
        ValidationResult result = createTransferValidator.isValid(new CreateTransfer(null, "12345678", "12345679", null, LocalDate.now(), BigDecimal.ONE));

        assertThat(result.isValid()).isTrue();
    }
}