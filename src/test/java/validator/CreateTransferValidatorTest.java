package validator;

import command.CreateTransfer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CreateTransferValidatorTest {

    private static final ValidationResult SUCCESS = new ValidationResult("", true);
    private static final ValidationResult FAILURE = new ValidationResult("", false);

    @Test
    void shouldReturnValidResultWhenNoValidatorsDefined() {
        Map<String, Validator> validators = Map.of();

        ValidationResult validationResult = new CreateTransferValidator(validators).isValid(getCreateTransfer());

        assertThat(validationResult.isValid()).isTrue();
    }

    @Test
    void shouldReturnValidResultWhenAllValidatorsReturnedValidResult(@Mock Validator<Object> validator) {
        when(validator.isValid(any())).thenReturn(SUCCESS);
        Map<String, Validator> validators = Map.of(
                "recipient", validator,
                "account", validator,
                "reference", validator,
                "settlementDate", validator,
                "amount", validator);

        ValidationResult validationResult = new CreateTransferValidator(validators).isValid(getCreateTransfer());

        assertThat(validationResult.isValid()).isTrue();
        verify(validator, times(validators.size())).isValid(any());
    }

    @Test
    void shouldReturnNotValidResultWhenOneValidatorsReturnedValidResult(@Mock Validator<Object> validator) {
        when(validator.isValid(any())).thenReturn(FAILURE).thenReturn(SUCCESS);
        Map<String, Validator> validators = Map.of(
                "recipient", validator,
                "account", validator);

        ValidationResult validationResult = new CreateTransferValidator(validators).isValid(getCreateTransfer());

        assertThat(validationResult.isValid()).isFalse();
        verify(validator, times(validators.size())).isValid(any());
    }

    private CreateTransfer getCreateTransfer() {
        return new CreateTransfer(null, null, null, null, null, null);
    }
}