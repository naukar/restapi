import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TransferPropertiesTest {

    private TransferProperties testProperties = new TransferProperties("testProperties");

    @Test
    void shouldReadIntegerPropertyFromAFile() throws IOException {
        Integer integerProperty = testProperties.getInteger("integer");
        assertThat(integerProperty).isEqualTo(1);
    }

    @Test
    void shouldFailToReadIntegerPropertyFromAFile() {
        assertThrows(NumberFormatException.class, () -> testProperties.getInteger("text"));
    }
}