package gateway;

import domain.Transfer;
import domain.events.TransferSubmitted;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.BalanceRepository;
import repository.Repository;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClearingTransferGatewayTest {

    @Test
    void shouldUpdateBalanceForBothAccountsWhenNewTransactionEventGotProcessed(
            @Mock Transfer transfer, @Mock BalanceRepository balanceRepository, @Mock Repository<Transfer> transferRepository) {
        int transferId = 1;
        String account = "a1";
        String recipientAccount = "a2";
        BigDecimal amount = BigDecimal.ONE;
        when(transferRepository.findById(1)).thenReturn(transfer);
        when(transfer.getAccount()).thenReturn(account);
        when(transfer.getRecipientAccount()).thenReturn(recipientAccount);
        when(transfer.getAmount()).thenReturn(amount);

        new ClearingTransferGateway(balanceRepository, transferRepository).onNext(new TransferSubmitted(transferId));

        verify(transferRepository).findById(transferId);
        verify(balanceRepository).changeBy(account, amount.negate());
        verify(balanceRepository).changeBy(recipientAccount, amount);
        verifyNoMoreInteractions(balanceRepository);
    }
}