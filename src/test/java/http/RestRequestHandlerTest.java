package http;

import com.google.common.collect.Maps;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static http.Response.*;
import static http.RestAction.*;
import static http.RestRequestHandler.METHOD_GET;
import static http.RestRequestHandler.METHOD_POST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class RestRequestHandlerTest {

    private static final String BASE_URL = "/base";
    private static final String REQUEST_BODY = "";

    @Captor
    private ArgumentCaptor<Request> requestArgumentCaptor;
    @Mock
    private RequestProcessor createProcessor;
    @Mock
    private RequestProcessor listProcessor;
    @Mock
    private RequestProcessor getProcessor;
    @Mock
    private Headers headers;

    private RestRequestHandler restRequestHandler;

    @BeforeEach
    void init() {
        restRequestHandler = new RestRequestHandler(
                Map.of(CREATE, createProcessor, LIST, listProcessor, GET, getProcessor),
                BASE_URL);
    }

    @Test
    void shouldCallProcessorForActionCreateWhenMethodIsPost(@Mock HttpExchange exchange) throws Exception {
        mockAnExchangeWithRequestMethod(exchange, METHOD_POST, BASE_URL);
        when(createProcessor.process(any(Request.class))).thenReturn(success(""));

        restRequestHandler.handle(exchange);

        verify(createProcessor).process(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).isEqualTo(new Request(REQUEST_BODY, Maps.newHashMap()));
        verifyNoInteractions(listProcessor, getProcessor);
    }

    @Test
    void shouldCallProcessorForActionListWhenMethodIsPost(@Mock HttpExchange exchange) throws Exception {
        mockAnExchangeWithRequestMethod(exchange, METHOD_GET, BASE_URL);
        when(listProcessor.process(any(Request.class))).thenReturn(success(""));

        restRequestHandler.handle(exchange);

        verify(listProcessor).process(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).isEqualTo(new Request(REQUEST_BODY, Maps.newHashMap()));
        verifyNoInteractions(createProcessor, getProcessor);
    }

    @Test
    void shouldCallProcessorForActionGetWhenMethodIsGetAndParamIdPassed(@Mock HttpExchange exchange) throws Exception {
        mockAnExchangeWithRequestMethod(exchange, METHOD_GET, BASE_URL + "/1");
        when(getProcessor.process(any(Request.class))).thenReturn(success(""));

        restRequestHandler.handle(exchange);

        verify(getProcessor).process(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).isEqualTo(new Request(REQUEST_BODY, Map.of("id", "1")));
        verifyNoInteractions(createProcessor, listProcessor);
    }

    @Test
    void shouldNotCallAnyProcessorWhenMethodIsGetAndParamIdPassed(@Mock HttpExchange exchange) throws Exception {
        mockAnExchangeWithRequestMethod(exchange, "OPTIONS", BASE_URL);

        restRequestHandler.handle(exchange);

        verifyNoInteractions(createProcessor, listProcessor, getProcessor);
    }

    @Test
    void shouldReturnInternalErrorWhenExceptionisThrown(@Mock HttpExchange exchange) throws Exception {
        mockAnExchangeWithRequestMethod(exchange, METHOD_GET, BASE_URL);
        when(listProcessor.process(any())).thenThrow(RuntimeException.class);

        restRequestHandler.handle(exchange);

        verify(exchange).sendResponseHeaders(eq(INTERNAL_ERROR), anyLong());
    }

    @Test
    void shouldReturnResponseReturnedByProccessor(@Mock HttpExchange exchange) throws Exception {
        mockAnExchangeWithRequestMethod(exchange, METHOD_GET, BASE_URL);
        when(listProcessor.process(any(Request.class))).thenReturn(success(""));

        restRequestHandler.handle(exchange);

        verify(headers).put("Content-Type", Lists.newArrayList("application/json"));
        verify(exchange).sendResponseHeaders(SUCCESS, 0);
    }

    private void mockAnExchangeWithRequestMethod(HttpExchange exchange, String method, String url) throws URISyntaxException {
        when(exchange.getRequestMethod()).thenReturn(method);
        when(exchange.getResponseBody()).thenReturn(new ByteArrayOutputStream());
        when(exchange.getRequestBody()).thenReturn(new ByteArrayInputStream("".getBytes()));
        when(exchange.getRequestURI()).thenReturn(new URI(url));
        when(exchange.getResponseHeaders()).thenReturn(headers);
    }
}