package http;

import controller.TransferController;
import converter.Converter;
import domain.Transfer;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static http.Response.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetTransfersProcessorTest {

    @Test
    void shouldGetAllTheObjectsAndConvertThemIntoSuccessJson(@Mock TransferController controller,
                                                             @Mock Converter<List<Transfer>, String> converter,
                                                             @Mock Request request) throws Exception {
        List<Transfer> responseList = Lists.emptyList();
        when(controller.getAll()).thenReturn(new Response<>(SUCCESS, responseList));
        String expectedResponse = "[]";
        when(converter.convert(responseList)).thenReturn(expectedResponse);

        Response<String> response = new GetTransfersProcessor(controller, converter).process(request);

        assertThat(response).isEqualTo(Response.success(expectedResponse));
        verify(controller).getAll();
        verify(converter).convert(responseList);
    }

}