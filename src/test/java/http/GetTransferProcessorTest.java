package http;

import controller.TransferController;
import converter.ConversionException;
import converter.Converter;
import domain.Transfer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class GetTransferProcessorTest {

    private GetTransferProcessor processor;
    private TransferController controller;
    private Converter<Transfer, String> jsonConverter;
    private Converter<String, Integer> integerConverter;

    @BeforeEach
    void init(@Mock TransferController controller,
              @Mock Converter<Transfer, String> jsonConverter,
              @Mock Converter<String, Integer> integerConverter) {
        this.controller = controller;
        this.jsonConverter = jsonConverter;
        this.integerConverter = integerConverter;
        processor = new GetTransferProcessor(controller, jsonConverter, integerConverter);
    }

    @Test
    void shouldReturnSuccessWhenRequestIsProperlyConvertedAndObjectWasFound(@Mock Request request, @Mock Transfer transfer) throws Exception {
        String idParam = "1";
        int id = 1;
        when(request.getParams()).thenReturn(Map.of("id", idParam));
        when(integerConverter.convert(request.getParams().get("id"))).thenReturn(id);
        when(controller.get(1)).thenReturn(Optional.of(transfer));
        String expectedResponse = "transfer";
        when(jsonConverter.convert(transfer)).thenReturn(expectedResponse);

        Response<String> process = processor.process(request);

        assertThat(process).isEqualTo(Response.success(expectedResponse));
        InOrder inOrder = Mockito.inOrder(integerConverter, controller, jsonConverter);
        inOrder.verify(integerConverter).convert(idParam);
        inOrder.verify(controller).get(id);
        inOrder.verify(jsonConverter).convert(transfer);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void shouldReturnBadRequestWhenIdPassedIsNotProper(@Mock Request request) throws Exception {
        when(integerConverter.convert(anyString())).thenThrow(ConversionException.class);

        Response<String> process = processor.process(request);

        assertThat(process).isEqualTo(Response.badRequest());
    }

    @Test
    void shouldReturnNotFoundWhenObjectDoesNotExist(@Mock Request request) throws Exception {
        when(request.getParams()).thenReturn(Map.of("id", "1"));
        when(integerConverter.convert(request.getParams().get("id"))).thenReturn(1);
        when(controller.get(1)).thenReturn(Optional.empty());

        Response<String> process = processor.process(request);

        assertThat(process).isEqualTo(Response.notFound());
        verifyNoInteractions(jsonConverter);
    }
}