package http;

import command.CreateTransfer;
import controller.TransferController;
import converter.ConversionException;
import converter.Converter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class CreateTransferProcessorTest {

    private CreateTransferProcessor createTransferProcessor;
    private TransferController controller;
    private Converter<String, CreateTransfer> converter;

    @BeforeEach
    void init(@Mock TransferController controller, @Mock Converter<String, CreateTransfer> converter) {
        this.controller = controller;
        this.converter = converter;
        createTransferProcessor = new CreateTransferProcessor(controller, converter);
    }

    @Test
    void shouldProcessSuccessfullyRequest(@Mock Request request, @Mock CreateTransfer createTransfer) throws Exception {
        Response<String> expectedResponse = Response.success("success");
        when(converter.convert(request.getBody())).thenReturn(createTransfer);
        when(controller.create(createTransfer)).thenReturn(expectedResponse);

        Response<String> response = createTransferProcessor.process(request);

        verify(controller).create(createTransfer);
        assertThat(response).isEqualTo(expectedResponse);
    }

    @Test
    void shouldReturnBadRequestWhenFailedToConvertRequestBody(@Mock Request request) throws Exception {
        when(converter.convert(request.getBody())).thenThrow(ConversionException.class);

        Response<String> response = createTransferProcessor.process(request);

        verify(controller, never()).create(any());
        assertThat(response).isEqualTo(Response.badRequest());
    }
}