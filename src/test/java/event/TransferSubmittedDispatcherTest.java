package event;

import domain.events.TransferSubmitted;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.SubmissionPublisher;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TransferSubmittedDispatcherTest {

    @Test
    void shouldSubmitTransferSubmittedEventThroughSubmissionPublisher(@Mock SubmissionPublisher<TransferSubmitted> publisher) {
        TransferSubmitted event = new TransferSubmitted(1);

        new TransferSubmittedDispatcher(publisher).submit(event);

        verify(publisher).submit(event);
    }
}