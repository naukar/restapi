package command.handler;

import command.CreateTransfer;
import domain.Transfer;
import domain.events.TransferSubmitted;
import event.EventDispatcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.Repository;
import validator.ValidationResult;
import validator.Validator;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class CreateTransferHandlerTest {

    private static final String ACCOUNT = "788998";
    private static final BigDecimal AMOUNT = BigDecimal.ONE;
    private static final String RECIPIENT = "REC";
    private static final String REFERENCE = "REFER";
    private static final LocalDate SETTLEMENT_DATE = LocalDate.parse("2019-01-02");
    private static final String RECIPIENT_ACCOUNT = "874534";
    private CreateTransferHandler handler;
    private Validator<CreateTransfer> validator;
    private Repository<Transfer> repository;
    private EventDispatcher<TransferSubmitted> eventDispatcher;

    @BeforeEach
    void init(@Mock Validator<CreateTransfer> validator,
              @Mock Repository<Transfer> repository,
              @Mock EventDispatcher<TransferSubmitted> eventDispatcher) {
        this.validator = validator;
        this.repository = repository;
        this.eventDispatcher = eventDispatcher;
        handler = new CreateTransferHandler(validator, repository, eventDispatcher);
    }

    @Test
    void shouldReturnFailureWhenPassedObjectIsNotValid(@Mock CreateTransfer createTransfer,
                                                       @Mock ValidationResult validationResult) {
        when(validator.isValid(createTransfer)).thenReturn(validationResult);
        when(validationResult.isValid()).thenReturn(false);

        CreateTransferResult result = handler.handle(createTransfer);

        verify(validator).isValid(createTransfer);
        verify(eventDispatcher, never()).submit(any());
        assertThat(result.isSuccessful()).isFalse();
    }

    @Test
    void shouldReturnSuccessWhenPassedObjectIsValid(@Mock CreateTransfer createTransfer,
                                                       @Mock ValidationResult validationResult) {
        when(createTransfer.getAccount()).thenReturn(ACCOUNT);
        when(createTransfer.getRecipientAccount()).thenReturn(RECIPIENT_ACCOUNT);
        when(createTransfer.getAmount()).thenReturn(AMOUNT);
        when(createTransfer.getRecipient()).thenReturn(RECIPIENT);
        when(createTransfer.getReference()).thenReturn(REFERENCE);
        when(createTransfer.getSettlementDate()).thenReturn(SETTLEMENT_DATE);

        when(validator.isValid(createTransfer)).thenReturn(validationResult);
        when(validationResult.isValid()).thenReturn(true);

        CreateTransferResult result = handler.handle(createTransfer);

        verify(validator).isValid(createTransfer);
        verify(repository).save(new Transfer(
                ACCOUNT, RECIPIENT_ACCOUNT, AMOUNT, SETTLEMENT_DATE, RECIPIENT, REFERENCE
        ));
        verify(eventDispatcher).submit(any());
        assertThat(result.isSuccessful()).isTrue();
    }
}