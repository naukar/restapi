package converter;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IntegerConverterTest {

    @Test
    void shouldConvertStringToInteger() throws ConversionException {
        Integer converted = new IntegerConverter().convert("1");

        assertThat(converted).isEqualTo(1);
    }

    @Test
    void shouldThrowConversionExceptionWhenNotProperStringPassed() throws ConversionException {
        assertThrows(ConversionException.class, () -> new IntegerConverter().convert("a"));
    }
}