package converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import command.CreateTransfer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ToJsonConverterTest {

    @Test
    void shouldConvertObjectToStringUsingMapper(@Mock ObjectMapper mapper) throws Exception {
        Object object = new Object();
        new ToJsonConverter<>(mapper).convert(object);

        verify(mapper).writeValueAsString(object);
    }

    @Test
    void shouldThrowConversionExceptionWhenMessageIsWrong(@Mock ObjectMapper mapper) throws JsonProcessingException {
        Object object = new Object();
        when(mapper.writeValueAsString(object)).thenThrow(JsonProcessingException.class);
        assertThrows(ConversionException.class, () ->
                new ToJsonConverter<>(mapper).convert(object));
    }
}