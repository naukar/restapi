package converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import command.CreateTransfer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FromJsonConverterTest {

    @Test
    void shouldConvertStringToCreateTransferObject(@Mock ObjectMapper mapper) throws Exception {
        String message = "message";
        new FromJsonConverter<>(mapper, CreateTransfer.class).convert(message);

        verify(mapper).readValue(message, CreateTransfer.class);
    }

    @Test
    void shouldThrowConversionExceptionWhenMessageIsWrong(@Mock ObjectMapper mapper) throws JsonProcessingException {
        String message = "message";
        when(mapper.readValue("message", CreateTransfer.class)).thenThrow(JsonProcessingException.class);
        assertThrows(ConversionException.class, () ->
                new FromJsonConverter<>(mapper, CreateTransfer.class).convert(message));
    }
}