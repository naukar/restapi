FROM adoptopenjdk/openjdk11

WORKDIR /usr/src/app/

ADD ./target/classes/transfer-jar-with-dependencies.jar .

RUN ls -ltr .

EXPOSE 8000

CMD java -jar /usr/src/app/transfer-jar-with-dependencies.jar
